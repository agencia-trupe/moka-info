@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Atendimento</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.atendimento.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.atendimento.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
