@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('fone', 'Fone') !!}
    {!! Form::text('fone', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('e_mail', 'E-mail') !!}
    {!! Form::text('e_mail', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('endereco', 'Endereço') !!}
    {!! Form::text('endereco', null, ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
