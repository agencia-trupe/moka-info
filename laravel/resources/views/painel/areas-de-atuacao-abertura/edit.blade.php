@extends('painel.common.template')

@section('content')

    <a href="{{ route('painel.areas-de-atuacao.index') }}" title="Voltar para Áreas de Atuação" class="btn btn-sm btn-default">
        &larr; Voltar para Áreas de Atuação
    </a>

    <legend>
        <h2><small>Áreas De Atuação /</small> Abertura</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.areas-de-atuacao-abertura.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.areas-de-atuacao-abertura.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
