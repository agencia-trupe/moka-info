@include('painel.common.flash')

<div class="well form-group">
    {!! Form::label('icone', 'Ícone') !!}
@if($submitText == 'Alterar')
    <img src="{{ url('assets/img/areas-de-atuacao/'.$registro->icone) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
@endif
    {!! Form::file('icone', ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('titulo', 'Título') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto', 'Texto') !!}
    {!! Form::textarea('texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'atuacao']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.areas-de-atuacao.index') }}" class="btn btn-default btn-voltar">Voltar</a>
