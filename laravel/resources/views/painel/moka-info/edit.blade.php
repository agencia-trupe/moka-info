@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Moka Info</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.moka-info.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.moka-info.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
