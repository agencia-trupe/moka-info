    <header>
        <div class="center">
            <nav id="desktop">
                <h1>
                    <a class="logo" href="{{ route('home') }}">
                        <img src="{{ asset('assets/img/layout/moka-info.png') }}" alt="{{ config('site.name') }}">
                    </a>
                </h1>

                <a class="link @if(Route::currentRouteName() == 'info') active @endif" href="{{ route('info') }}">Moka Info</a>
                <a class="link @if(Route::currentRouteName() == 'atuacao') active @endif" href="{{ route('atuacao') }}">Áreas de Atuação</a>
                <a class="link @if(Route::currentRouteName() == 'atendimento') active @endif" href="{{ route('atendimento') }}">Atendimento</a>

                <a href="http://www.mokainvest.com.br/" class="link externo">Moka Invest</a>
                <a href="http://www.mokagestora.com.br/" class="link">Moka Gestora</a>
            </nav>

            <div id="handle-mobile">
                <h1>
                    <a class="logo" href="{{ route('home') }}">
                        <img src="{{ asset('assets/img/layout/moka-info.png') }}" alt="{{ config('site.name') }}">
                    </a>
                </h1>

                <button id="mobile-toggle" type="button" role="button">
                    <span class="lines"></span>
                </button>
            </div>
        </div>

        <div class="login">
            <div class="center">
                <form action="#" method="post">
                    <input name="login" type="text" placeholder="Usuário" required>
                    <input name="senha" type="password" placeholder="Senha" required>
                    <input type="submit" value="OK">
                </form>
            </div>
        </div>
    </header>

    <nav id="mobile">
        <a class="link @if(Route::currentRouteName() == 'info') active @endif" href="{{ route('info') }}">Moka Info</a>
        <a class="link @if(Route::currentRouteName() == 'atuacao') active @endif" href="{{ route('atuacao') }}">Áreas de Atuação</a>
        <a class="link @if(Route::currentRouteName() == 'atendimento') active @endif" href="{{ route('atendimento') }}">Atendimento</a>

        <a href="http://www.mokainvest.com.br/" class="link externo">Moka Invest</a>
        <a href="http://www.mokagestora.com.br/" class="link">Moka Gestora</a>
    </nav>
