    <footer>
        <div class="center">
            <p><span>{{ config('site.name') }}</span> - {{ $contato->endereco }} • {{ $contato->fone }} • <a href="mailto:{{ $contato->e_mail }}">{{ $contato->e_mail }}</a></p>
        </div>
    </footer>
