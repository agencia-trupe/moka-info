@extends('frontend.common.template')

@section('content')

    <div class="center">
        <div class="main">
            <h1>MOKA INFO</h1>
            <div>
                {!! $info->texto !!}
            </div>
            <img src="{{ asset('assets/img/moka-info/'.$info->imagem) }}" class="info-imagem" alt="">
        </div>
    </div>

@endsection
