@extends('frontend.common.template')

@section('content')

    <div class="center">
        <div class="main">
            <h1>ATENDIMENTO</h1>
            <h2>MOKA INFO</h2>
            <p>FONE: {{ $atendimento->fone }}</p>
            <p>E-mail: <a href="mailto:{{ $atendimento->e_mail }}">{{ $atendimento->e_mail }}</a></p>
            <p>{{ $atendimento->endereco }}</p>
        </div>
    </div>

@endsection
