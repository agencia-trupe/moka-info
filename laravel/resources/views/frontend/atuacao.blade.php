@extends('frontend.common.template')

@section('content')

    <div class="center">
        <div class="main atuacao">
            <h2>ÁREAS DE ATUAÇÃO</h2>
            <h1 class="titulo">{{ $abertura->titulo }}</h1>
            {!! $abertura->texto !!}
            <h3>{{ $abertura->subtitulo }}</h3>

            <h1>INDÚSTRIA FARMACÊUTICA</h1>
            @foreach($areas as $area)
            <div class="area">
                <div class="icone"><img src="{{ asset('assets/img/areas-de-atuacao/'.$area->icone) }}" alt=""></div>
                <div class="texto">
                    <a href="#" class="handle">{{ $area->titulo }}</a>
                    <div class="descricao">
                        {!! $area->texto !!}
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>

@endsection
