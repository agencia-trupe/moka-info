<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AtendimentoRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'fone' => 'required',
            'e_mail' => 'required|email',
            'endereco' => 'required',
        ];
    }
}
