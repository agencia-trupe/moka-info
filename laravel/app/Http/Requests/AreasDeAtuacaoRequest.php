<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AreasDeAtuacaoRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'icone' => 'required|image',
            'titulo' => 'required',
            'texto' => 'required',
        ];

        if ($this->method() != 'POST') {
            $rules['icone'] = 'image';
        }

        return $rules;
    }
}
