<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\AtendimentoRequest;
use App\Http\Controllers\Controller;

use App\Models\Atendimento;

class AtendimentoController extends Controller
{
    public function index()
    {
        $registro = Atendimento::first();

        return view('painel.atendimento.edit', compact('registro'));
    }

    public function update(AtendimentoRequest $request, Atendimento $registro)
    {
        try {
            $input = $request->all();


            $registro->update($input);

            return redirect()->route('painel.atendimento.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
