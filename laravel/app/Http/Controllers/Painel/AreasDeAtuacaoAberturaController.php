<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\AreasDeAtuacaoAberturaRequest;
use App\Http\Controllers\Controller;

use App\Models\AreasDeAtuacaoAbertura;

class AreasDeAtuacaoAberturaController extends Controller
{
    public function index()
    {
        $registro = AreasDeAtuacaoAbertura::first();

        return view('painel.areas-de-atuacao-abertura.edit', compact('registro'));
    }

    public function update(AreasDeAtuacaoAberturaRequest $request, AreasDeAtuacaoAbertura $registro)
    {
        try {
            $input = $request->all();


            $registro->update($input);

            return redirect()->route('painel.areas-de-atuacao-abertura.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
