<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\MokaInfoRequest;
use App\Http\Controllers\Controller;

use App\Models\MokaInfo;

class MokaInfoController extends Controller
{
    public function index()
    {
        $registro = MokaInfo::first();

        return view('painel.moka-info.edit', compact('registro'));
    }

    public function update(MokaInfoRequest $request, MokaInfo $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = MokaInfo::upload_imagem();

            $registro->update($input);

            return redirect()->route('painel.moka-info.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
