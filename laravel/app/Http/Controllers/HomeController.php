<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Banner;
use App\Models\MokaInfo;
use App\Models\AreasDeAtuacaoAbertura;
use App\Models\AreaDeAtuacao;
use App\Models\Atendimento;

class HomeController extends Controller
{
    public function index()
    {
        $banners = Banner::ordenados()->get();

        return view('frontend.home', compact('banners'));
    }

    public function info()
    {
        $info = MokaInfo::first();

        return view('frontend.info', compact('info'));
    }

    public function atuacao()
    {
        $abertura = AreasDeAtuacaoAbertura::first();
        $areas = AreaDeAtuacao::ordenados()->get();

        return view('frontend.atuacao', compact('abertura', 'areas'));
    }

    public function atendimento()
    {
        $atendimento = Atendimento::first();

        return view('frontend.atendimento', compact('atendimento'));
    }
}
