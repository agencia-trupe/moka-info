<?php

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('moka-info', 'HomeController@info')->name('info');
    Route::get('areas-de-atuacao', 'HomeController@atuacao')->name('atuacao');
    Route::get('atendimento', 'HomeController@atendimento')->name('atendimento');

    // Painel
    Route::group([
        'prefix'     => 'painel',
        'namespace'  => 'Painel',
        'middleware' => ['auth']
    ], function() {
        Route::get('/', 'PainelController@index')->name('painel');

        /* GENERATED ROUTES */
		Route::resource('areas-de-atuacao-abertura', 'AreasDeAtuacaoAberturaController', ['only' => ['index', 'update']]);
		Route::resource('areas-de-atuacao', 'AreasDeAtuacaoController');
		Route::resource('moka-info', 'MokaInfoController', ['only' => ['index', 'update']]);
		Route::resource('atendimento', 'AtendimentoController', ['only' => ['index', 'update']]);
		Route::resource('banners', 'BannersController');
        // Route::resource('contato/recebidos', 'ContatosRecebidosController');
        // Route::resource('contato', 'ContatoController');
        Route::resource('usuarios', 'UsuariosController');

        Route::post('order', 'PainelController@order');
        Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
    });

    // Auth
    Route::group([
        'prefix'    => 'painel',
        'namespace' => 'Auth'
    ], function() {
        Route::get('login', 'AuthController@showLoginForm')->name('auth');
        Route::post('login', 'AuthController@login')->name('login');
        Route::get('logout', 'AuthController@logout')->name('logout');
    });
});
