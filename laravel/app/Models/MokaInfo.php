<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class MokaInfo extends Model
{
    protected $table = 'moka_info';

    protected $guarded = ['id'];

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'  => null,
            'height' => null,
            'path'   => 'assets/img/moka-info/'
        ]);
    }

}
