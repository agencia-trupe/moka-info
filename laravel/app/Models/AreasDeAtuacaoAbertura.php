<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class AreasDeAtuacaoAbertura extends Model
{
    protected $table = 'areas_de_atuacao_abertura';

    protected $guarded = ['id'];

}
