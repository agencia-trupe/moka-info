<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class AreaDeAtuacao extends Model
{
    protected $table = 'areas_de_atuacao';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function upload_icone()
    {
        return CropImage::make('icone', [
            'width'  => null,
            'height' => null,
            'path'   => 'assets/img/areas-de-atuacao/'
        ]);
    }

}
