<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Atendimento extends Model
{
    protected $table = 'atendimento';

    protected $guarded = ['id'];

}
