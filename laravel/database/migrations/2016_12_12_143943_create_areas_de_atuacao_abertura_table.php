<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAreasDeAtuacaoAberturaTable extends Migration
{
    public function up()
    {
        Schema::create('areas_de_atuacao_abertura', function (Blueprint $table) {
            $table->increments('id');
            $table->string('titulo');
            $table->text('texto');
            $table->string('subtitulo');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('areas_de_atuacao_abertura');
    }
}
