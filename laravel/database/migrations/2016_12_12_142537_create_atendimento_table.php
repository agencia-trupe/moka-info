<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAtendimentoTable extends Migration
{
    public function up()
    {
        Schema::create('atendimento', function (Blueprint $table) {
            $table->increments('id');
            $table->string('fone');
            $table->string('e_mail');
            $table->text('endereco');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('atendimento');
    }
}
