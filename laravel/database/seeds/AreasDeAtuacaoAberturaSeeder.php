<?php

use Illuminate\Database\Seeder;

class AreasDeAtuacaoAberturaSeeder extends Seeder
{
    public function run()
    {
        DB::table('areas_de_atuacao_abertura')->insert([
            'titulo' => '',
            'texto' => '',
            'subtitulo' => '',
        ]);
    }
}
