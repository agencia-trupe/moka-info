-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Tempo de geração: 12/12/2016 às 15:10
-- Versão do servidor: 5.7.10
-- Versão do PHP: 7.0.1-1+deb.sury.org~trusty+2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `mokainfo`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `areas_de_atuacao`
--

CREATE TABLE `areas_de_atuacao` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `icone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `areas_de_atuacao`
--

INSERT INTO `areas_de_atuacao` (`id`, `ordem`, `icone`, `titulo`, `texto`, `created_at`, `updated_at`) VALUES
(1, 1, 'icone-cursos_20161212150348.png', 'CURSOS', '<p>Ampliar seu conhecimento sobre o sistema de sa&uacute;de brasileiro entendendo como usar informa&ccedil;&otilde;es &agrave; seu favor.</p>\r\n\r\n<p><a href="#">O SISTEMA SA&Uacute;DE</a></p>\r\n\r\n<p><a href="#">INFORMA&Ccedil;&Otilde;ES EM SA&Uacute;DE</a></p>\r\n', '2016-12-12 15:03:48', '2016-12-12 15:06:08'),
(2, 2, 'icone-analises_20161212150705.png', 'ANÁLISES SUS | ANALISASUS', '<p>Otimizar esfor&ccedil;os promocionais e acesso aos medicamentos pelos pacientes atrav&eacute;s do monitoramento de&nbsp;informa&ccedil;&otilde;es disponibilizadas pelos bancos de dados gerenciados pelo DATASUS, principalmente do&nbsp;Componente Especializado da Assist&ecirc;ncia Farmac&ecirc;utica e da Oncologia.</p>\r\n', '2016-12-12 15:07:05', '2016-12-12 15:07:05'),
(3, 3, 'icone-cadastro_20161212150739.png', 'CADASTRO DE ESTABELECIMENTOS | ESTABELECE', '<p>Alimentar cadastro de for&ccedil;a de vendas, suporte em estudos de targeting / segmenta&ccedil;&atilde;o e dimensionamento&nbsp;de mercado. Informa&ccedil;&otilde;es de mais de 300 mil estabelecimentos de sa&uacute;de no pa&iacute;s de fonte oficial e p&uacute;blica.</p>\r\n', '2016-12-12 15:07:40', '2016-12-12 15:07:40'),
(4, 4, 'icone-oncologistas_20161212150811.png', 'ONCOLOGISTAS EM AÇÃO | ONCOREF', '<p>Enriquecer seu cadastro m&eacute;dico em Oncologia atrav&eacute;s da an&aacute;lise dos atendimentos m&eacute;dicos em centros de&nbsp;refer&ecirc;ncias do SUS.</p>\r\n', '2016-12-12 15:08:11', '2016-12-12 15:08:11'),
(5, 5, 'icone-pesquisapacientes_20161212150854.png', 'PESQUISA DE PACIENTES | RARAS', '<p>Antecipar o diagn&oacute;stico de pacientes com doen&ccedil;as raras atrav&eacute;s da associa&ccedil;&atilde;o de sintomas, diagn&oacute;sticos e&nbsp;tratamentos de pacientes no SUS. Trazemos os locais de tratamento desses pacientes, bem como os m&eacute;dicos.</p>\r\n', '2016-12-12 15:08:54', '2016-12-12 15:08:54'),
(6, 6, 'icone-consumo_20161212150923.png', 'CONSUMO DE RECURSOS E PADRÃO DE TRATAMENTO | TRATAMAIS', '<p>Gerar publica&ccedil;&otilde;es e argumentos de Economia da Sa&uacute;de, que poder&atilde;o ser utilizados nas negocia&ccedil;&otilde;es com&nbsp;Secretarias ou submiss&atilde;o &agrave; CONITEC. Organizamos as informa&ccedil;&otilde;es do SUS a fim de identificar o padr&atilde;o de&nbsp;consumo de recursos e tratamentos de determinado diagn&oacute;stico no Brasil.</p>\r\n', '2016-12-12 15:09:23', '2016-12-12 15:09:23'),
(7, 7, 'icone-sus_20161212150945.png', 'SUS + PROGRAMA DE PACIENTES | ACOMPANHASUS', '<p>Ter mais informa&ccedil;&otilde;es de seus pacientes, permitindo a&ccedil;&otilde;es mais assertivas para sua manuten&ccedil;&atilde;o e tratamento.&nbsp;Para pacientes em tratamento no Componente Especializado da Assist&ecirc;ncia Farmac&ecirc;utica ou Oncologia do SUS&nbsp;atrav&eacute;s da integra&ccedil;&atilde;o dos dados do SUS ao dados do Programa de Pacientes do medicamento.</p>\r\n', '2016-12-12 15:09:45', '2016-12-12 15:09:45'),
(8, 8, 'icone-consultoria_20161212151017.png', 'CONSULTORIA DE INFORMAÇÕES | INFOCO', '<p>Ampliar o volume de informa&ccedil;&otilde;es dispon&iacute;veis na sua empresa, principalmente quando estiver ingressando&nbsp;em nova &aacute;rea terap&ecirc;utica, ou nicho de mercado. Fazemos isso atrav&eacute;s de pesquisa, avalia&ccedil;&atilde;o e qualifica&ccedil;&atilde;o&nbsp;de diferentes fontes de informa&ccedil;&otilde;es.</p>\r\n\r\n<p>Organizar as informa&ccedil;&otilde;es dispon&iacute;veis atrav&eacute;s de dashboards e KPIs.</p>\r\n\r\n<p>Alinhar o entendimento entres os envolvidos e estruturar para que seja um processo din&acirc;mico.</p>\r\n', '2016-12-12 15:10:17', '2016-12-12 15:10:17');

-- --------------------------------------------------------

--
-- Estrutura para tabela `areas_de_atuacao_abertura`
--

CREATE TABLE `areas_de_atuacao_abertura` (
  `id` int(10) UNSIGNED NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `subtitulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `areas_de_atuacao_abertura`
--

INSERT INTO `areas_de_atuacao_abertura` (`id`, `titulo`, `texto`, `subtitulo`, `created_at`, `updated_at`) VALUES
(1, 'SAÚDE', '<p>Ampliar o acesso &agrave; sa&uacute;de melhorando a aplicabilidade dos dados e do seu uso por todos os atores.&nbsp;Mergulharemos em diversas fontes de dados - dados secund&aacute;rios, pesquisa prim&aacute;ria, big data, dados do pr&oacute;prio cliente,&nbsp;de parceiros, entre outros - para gerar dashboards, relat&oacute;rios, cursos e publica&ccedil;&otilde;es, sempre seguindo rigorosos crit&eacute;rios de&nbsp;confidencialidade e seguran&ccedil;a das informa&ccedil;&otilde;es.</p>\r\n', 'FINANCIADORES | PAGADORES | FORNECEDORES | PRESTADORES | PACIENTES', NULL, '2016-12-12 14:58:29');

-- --------------------------------------------------------

--
-- Estrutura para tabela `atendimento`
--

CREATE TABLE `atendimento` (
  `id` int(10) UNSIGNED NOT NULL,
  `fone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `e_mail` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `endereco` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `atendimento`
--

INSERT INTO `atendimento` (`id`, `fone`, `e_mail`, `endereco`, `created_at`, `updated_at`) VALUES
(1, 'Tel 11 3138-5966', 'contato@mokainfo.com.br', 'Av. Angélica, 2118, 12o andar • 01228 200 • São Paulo SP', NULL, '2016-12-12 14:57:40');

-- --------------------------------------------------------

--
-- Estrutura para tabela `banners`
--

CREATE TABLE `banners` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `frase` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `banners`
--

INSERT INTO `banners` (`id`, `ordem`, `imagem`, `frase`, `created_at`, `updated_at`) VALUES
(1, 1, 'banner1_20161212145528.jpg', 'Organização e análise de dados', '2016-12-12 14:55:30', '2016-12-12 14:55:30'),
(2, 2, 'banner2_20161212145551.jpg', 'Disseminacão de informações', '2016-12-12 14:55:54', '2016-12-12 14:55:54'),
(3, 3, 'banner3_20161212145611.jpg', 'Dados do mundo real', '2016-12-12 14:56:12', '2016-12-12 14:56:12');

-- --------------------------------------------------------

--
-- Estrutura para tabela `contato`
--

CREATE TABLE `contato` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `contato`
--

INSERT INTO `contato` (`id`, `email`, `created_at`, `updated_at`) VALUES
(1, 'contato@trupe.net', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `contatos_recebidos`
--

CREATE TABLE `contatos_recebidos` (
  `id` int(10) UNSIGNED NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mensagem` text COLLATE utf8_unicode_ci NOT NULL,
  `lido` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2016_02_01_000000_create_users_table', 1),
('2016_03_01_000000_create_contato_table', 1),
('2016_03_01_000000_create_contatos_recebidos_table', 1),
('2016_12_12_141857_create_banners_table', 1),
('2016_12_12_142537_create_atendimento_table', 1),
('2016_12_12_142739_create_moka_info_table', 1),
('2016_12_12_143840_create_areas_de_atuacao_table', 1),
('2016_12_12_143943_create_areas_de_atuacao_abertura_table', 1);

-- --------------------------------------------------------

--
-- Estrutura para tabela `moka_info`
--

CREATE TABLE `moka_info` (
  `id` int(10) UNSIGNED NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `moka_info`
--

INSERT INTO `moka_info` (`id`, `texto`, `imagem`, `created_at`, `updated_at`) VALUES
(1, '<p>Integramos, organizamos e analisamos dados de diferentes fontes atrav&eacute;s de sofisticados recursos tecnol&oacute;gicos,&nbsp;disseminando-os para diferentes atores, a fim de facilitar suas intera&ccedil;&otilde;es atrav&eacute;s da amplia&ccedil;&atilde;o do conhecimento e da&nbsp;tomada de decis&otilde;es.</p>\r\n', 'grafico_20161212145651.png', NULL, '2016-12-12 14:56:51');

-- --------------------------------------------------------

--
-- Estrutura para tabela `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'trupe', 'contato@trupe.net', '$2y$10$8B/nAoWaOoeWmFznVjvV9uNFRTbO5CQtCbKSK1Ms5pdCL/TzoxzhC', NULL, NULL, NULL);

--
-- Índices de tabelas apagadas
--

--
-- Índices de tabela `areas_de_atuacao`
--
ALTER TABLE `areas_de_atuacao`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `areas_de_atuacao_abertura`
--
ALTER TABLE `areas_de_atuacao_abertura`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `atendimento`
--
ALTER TABLE `atendimento`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `contato`
--
ALTER TABLE `contato`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `contatos_recebidos`
--
ALTER TABLE `contatos_recebidos`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `moka_info`
--
ALTER TABLE `moka_info`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT de tabelas apagadas
--

--
-- AUTO_INCREMENT de tabela `areas_de_atuacao`
--
ALTER TABLE `areas_de_atuacao`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de tabela `areas_de_atuacao_abertura`
--
ALTER TABLE `areas_de_atuacao_abertura`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de tabela `atendimento`
--
ALTER TABLE `atendimento`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de tabela `banners`
--
ALTER TABLE `banners`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de tabela `contato`
--
ALTER TABLE `contato`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de tabela `contatos_recebidos`
--
ALTER TABLE `contatos_recebidos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de tabela `moka_info`
--
ALTER TABLE `moka_info`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de tabela `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
